const express = require('express');
require('dotenv').config();
const mongoose = require('mongoose');
const cookieParser = require("cookie-parser");

const cors = require('cors');

const app = express()
app.use(express.json());
app.use(cookieParser());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.get('/clients', (req, res) => {
    res.send('Hello World!')
    // Cookies that have not been signed
    console.log('Cookies: ', req.cookies)
})

app.listen(process.env.PORT, () => {
    console.log(`Example app listening on port ${process.env.PORT}`)
})

const routers = require('./src/routers/routers');
const authRoutes = require('./src/routers/authRoutes');
const verifyToken = require('./src/routers/validate-token');

app.use('/api/v1', verifyToken, routers);
app.use('/api/users', authRoutes);

mongoose.connect(process.env.MONGODB, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log('Conexión a MongoDB establecida'))
    .catch(err => console.log(err))