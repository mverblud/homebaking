const getAll = require('../repositories/user/userCase/getAccounts/getAccounts');
const createAccount = require('../repositories/user/userCase/createAccount/createAccount');

module.exports = {
    getAll,
    createAccount,
}