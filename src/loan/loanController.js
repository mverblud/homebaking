const getAll = require('../repositories/user/userCase/getLoans/getLoans');
const createClient = require('../repositories/user/userCase/createLoan/createLoan');

module.exports = {
    getAll,
    createClient,
}