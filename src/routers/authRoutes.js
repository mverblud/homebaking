const { Router } = require('express');
const router = new Router();

const userController = require('../user/userController');

router.post('/signup', userController.signup_post);
router.post('/login', userController.login_post);

module.exports = router;