const { Router } = require('express');
const router = new Router();

const { getClients, getClient } = require('../repositories/user/userCase/getClients/getClients');
const { createClient } = require('../repositories/user/userCase/createClient/createClient');

const { getAccounts, getAccount } = require('../repositories/user/userCase/getAccounts/getAccounts');
const { createAccount } = require('../repositories/user/userCase/createAccount/createAccount');

const { getTransactions, getTransaction } = require('../repositories/user/userCase/getTransactions/getTransactions');
const { createTransaction } = require('../repositories/user/userCase/createTransaction/createTransaction');

const { getLoans, getLoan } = require('../repositories/user/userCase/getLoans/getLoans');
const { createLoan } = require('../repositories/user/userCase/createLoan/createLoan');

const { getClientLoans, getClientLoan } = require('../repositories/user/userCase/getClientLoan/getClientLoan');
const { createClientLoan } = require('../repositories/user/userCase/createClientLoan/createClientLoan');

const { getCards, getCard } = require('../repositories/user/userCase/getCards/getCards');
const { createCard } = require('../repositories/user/userCase/createCard/createCard');

router.get('/clients', getClients);
router.get('/clients/:id', getClient);
router.post('/clients', createClient);

router.get('/accounts', getAccounts);
router.get('/accounts/:id', getAccount);
router.post('/accounts', createAccount);

router.get('/transactions', getTransactions);
router.get('/transactions/:id', getTransaction);
router.post('/transactions', createTransaction);

router.get('/loans', getLoans);
router.get('/loans/:id', getLoan);
router.post('/loans', createLoan);

router.get('/clientLoans', getClientLoans);
router.get('/clientLoans/:id', getClientLoan);
router.post('/clientLoans', createClientLoan);

router.get('/cards', getCards);
router.get('/cards/:id', getCard);
router.post('/cards', createCard);

module.exports = router;