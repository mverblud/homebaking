const jwt = require('jsonwebtoken');

const verifyToken = (req, res, next) => {
    //   const token = req.cookies["x-acces-token"];
    //   console.log(token);
    const token = req.header("auth-token");
    if (!token) {
        console.log('acceso denegado');
        return res.status(401).json({ error: "Acceso denegado" });
    }
    try {
        const verified = jwt.verify(token, process.env.SECRET_KEY);
        req.user = verified
        console.log('verificado');
        next()
    } catch (error) {
        console.log('token no valido');
        res.status(400).json({ error: "El token no es válido" })
    }

}

module.exports = verifyToken;