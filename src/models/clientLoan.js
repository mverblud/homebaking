const mongoose = require('mongoose');
const { Schema } = mongoose;

const clientLoanSchema = new mongoose.Schema({
    amount: { type: Number, required: true },
    payments: { type: Number, required: true },
    client: { type: mongoose.Schema.Types.ObjectId, ref: 'client' },
    loan: { type: mongoose.Schema.Types.ObjectId, ref: 'loan', },
});

module.exports = mongoose.model('clientLoan', clientLoanSchema);