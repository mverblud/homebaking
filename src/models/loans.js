const mongoose = require('mongoose');
const { Schema } = mongoose;

const loanSchema = new mongoose.Schema({
    name: { type: String, unique: true, required: true },
    maxAmount: { type: Number, default: 0, required: true },
    payments: [{ type: Number, required: true }],
    clientLoans: [{ type: mongoose.Schema.Types.ObjectId, ref: 'clientLoan' }],
});

module.exports = mongoose.model('loan', loanSchema);