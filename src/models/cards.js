const mongoose = require('mongoose');
const { Schema } = mongoose;

const cardSchema = new mongoose.Schema({
    cardHolder: { type: String, required: true },
    type: { type: String, },
    color: { type: String, },
    number: { type: String, },
    cvv: { type: Number, },
    thruDate: { type: Date, },
    fromDate: { type: Date, },
    client: { type: mongoose.Schema.Types.ObjectId, ref: 'client' },
});

module.exports = mongoose.model('card', cardSchema);