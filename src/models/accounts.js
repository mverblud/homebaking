const mongoose = require('mongoose');
const { Schema } = mongoose;

const accountSchema = new mongoose.Schema({
    number: { type: String, required: true },
    createDate: { type: Date, default: Date.now },
    balance: { type: Number, default: 0 },
    client: { type: mongoose.Schema.Types.ObjectId, ref: 'client' },
    transactions : [{type: mongoose.Schema.Types.ObjectId, ref: 'transaction'}]
});

module.exports = mongoose.model('account', accountSchema);
