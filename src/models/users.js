const mongoose = require('mongoose');
const { Schema } = mongoose;

const userSchema = new mongoose.Schema({
    firstName: { type: String, required: true, lowercase: true, },
    lastName: { type: String, required: true, lowercase: true, },
    email: { type: String, required: true, unique: true, lowercase: true, },
    password: { type: String, required: true, minlength: 6, },
});

module.exports = mongoose.model('user', userSchema);
