const mongoose = require('mongoose');
const { Schema } = mongoose;

const transactionSchema = new mongoose.Schema({
    type: { type: String, required: true},
    amount: { type: Number, default: 0 },
    description: { type: String, default: 'movimiento desconocido' },
    date: { type: Date, default: Date.now },
    account : {type: mongoose.Schema.Types.ObjectId, ref: 'account'},
});

module.exports = mongoose.model('transaction', transactionSchema);