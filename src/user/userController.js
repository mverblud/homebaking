const User = require('../models/users');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

module.exports.signup_post = async (req, res) => {

    const { firstName, lastName, email, password } = req.body

    // hash contraseña
    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(password, salt);

    try {
        const user = await User.create({ firstName, lastName, email, password: hashPassword })
        res.status(201).json(user);
    } catch (error) {
        res.status(400).json(error);
    }
};

module.exports.login_post = async (req, res) => {
    const { email, password } = req.body;
    const user = await User.findOne({ email: email });
    if (!user) {
        return res.status(400).json({ error: "usuario no encontrado" });
    }

    const validPassword = await bcrypt.compare(password, user.password);
    if (!validPassword) {
        return res.status(404).json({
            message: 'Contraseña incorrecta',
        })
    }

    // crear token
    const token = jwt.sign({
        name: user.email,
        id: user._id,
    }, process.env.SECRET_KEY, {
        expiresIn: 4000,
    })

 //   res.cookie("x-acces-token", token);
 //   const cookie = req.cookies;

    res.status(200).json({
        mensaje: `!Bienvenido, ${user.firstName} ${user.lastName}`,
        token,
   //     cookie
    })
}