const getAll = require('../repositories/user/userCase/getClients/getClients');
const createClient = require('../repositories/user/userCase/createClient/createClient');

module.exports = {
    getAll,
    createClient,
}