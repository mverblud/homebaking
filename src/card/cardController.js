const getAll = require('../repositories/user/userCase/getCards/getCards');
const createCard = require('../repositories/user/userCase/createCard/createCard');

module.exports = {
    getAll,
    createCard,
}