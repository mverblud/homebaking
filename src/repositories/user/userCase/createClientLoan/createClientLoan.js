const { response } = require('express');
const clientLoanRepository = require('../../../../repositories/clientLoanRepository');

const createClientLoan = async (req, res = response) => {

    try {
        await clientLoanRepository.save(req.body);

        return res.status(201).json({
            message: 'El client Loan se creo correctamente',
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Error Interno del Servidor',
            err: error
        })
    }
}

module.exports = {
    createClientLoan,
}