const { response } = require('express');
const clientLoanRepository = require('../../../../repositories/clientLoanRepository');

const getClientLoans = async (req, res = response) => {

    try {
        const clientLoans = await clientLoanRepository.getAll();
        const count = await clientLoanRepository.count();

        if (!clientLoans) {
            return res.status(401).json({
                message: 'Not found',
            })
        }

        res.status(200).json({
            message: 'clientLoans',
            response: clientLoans,
            total: count
        })

    } catch (error) {
        res.status(500).json({
            message: 'Error Interno del Servidor',
            err: error
        })
    }
}

const getClientLoan = async (req, res = response) => {
    console.log(req.params);
    const id = req.params.id;

    try {

        const clientLoan  = await clientLoanRepository.getOne(id);

        if (!clientLoan) {
            return res.status(401).json({
                message: 'Not found',
            })
        }

        res.status(200).json({
            message: 'clientLoan',
            response: clientLoan,
        })

    } catch (error) {
        res.status(500).json({
            message: 'Error Interno del Servidor',
            err: error
        })
    }
}

module.exports = {
    getClientLoans,
    getClientLoan,
}