const { response } = require('express');
const trasactionRepository = require('../../../../repositories/transactionRepository');

const createTransaction = async (req, res = response) => {

    try {
        await trasactionRepository.save(req.body);

        return res.status(201).json({
            message: 'La Transaccion se creo correctamente',
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Error Interno del Servidor',
            err: error
        })
    }
}

module.exports = {
    createTransaction,
}