const { response } = require('express');
const cardRepository = require('../../../../repositories/cardRepository');

const createCard = async (req, res = response) => {

    try {
        await cardRepository.save(req.body);

        return res.status(201).json({
            message: 'La card se creo correctamente',
        })

    } catch (error) {
        res.status(500).json({
            message: 'Error Interno del Servidor',
            err: error
        })
    }
}

module.exports = {
    createCard,
}