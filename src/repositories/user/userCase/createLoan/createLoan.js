const { response } = require('express');
const loanRepository = require('../../../../repositories/loanRepository');

const createLoan = async (req, res = response) => {

    try {
        await loanRepository.save(req.body);

        return res.status(201).json({
            message: 'Loan se creo correctamente',
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Error Interno del Servidor',
            err: error
        })
    }
}

module.exports = {
    createLoan,
}