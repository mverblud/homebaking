const Transaction = require('../models/transactions');
const Accounts = require('../models/accounts');
const TransactionType = require('../models/transactionType');

const getAll = async () => await Transaction.find();
const getOne = async (id) => await Transaction.findById(id);
const count = async () => await Transaction.count();

const save = async (body) => {

    const transaction = new Transaction({
        type: TransactionType.DEBIT,
        amount: body.amount,
        description: body.description,
        date: body.date,
        account: body.account,
    })

    const trn = await transaction.save();
    //  obtengo cuenta
    const account = await Accounts.findById(trn.account);
    
    account.transactions.push(trn._id);

    const cuentaActualidad = await Accounts.updateOne({ _id: account._id }, {transactions: account.transactions}); 
    
    return transaction;
}

module.exports = {
    getAll,
    getOne,
    count,
    save,
};