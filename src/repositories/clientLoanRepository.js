const ClientLoan = require('../models/clientLoan');
const Client = require('../models/clients');
const Loan = require('../models/loans');

const getAll = async () => await ClientLoan.find();
const getOne = async (id) => await ClientLoan.findById(id);
const count = async () => await ClientLoan.count();

const save = async (body) => {

    const clientLoan = new ClientLoan({
        amount: body.amount,
        payments: body.payments,
        client: body.client,
        loan: body.loan,
    })

    const clientLoanSaved = await clientLoan.save();
    const client = await Client.findById(clientLoanSaved.client);
    client.loans.push(clientLoanSaved._id);
    await Client.updateOne({ _id: client._id }, { loans: client.loans })

/*     const loan = await Loan.findById(clientLoanSaved.loan);
    console.log(loan);
    loan.loans.push(clientLoanSaved.id);
    await Loan.updateOne({ _id: loan._id }, { Loan: client.loans }) */

    return clientLoan;
}

module.exports = {
    getAll,
    getOne,
    count,
    save,
};