const Accounts = require('../models/accounts');
const Client = require('../models/clients');

const getAll = async () => await Accounts.find().populate('transactions');
const getOne = async (id) => await Accounts.findById(id).populate('transactions');
const count = async () => await Accounts.count();

const save = async (body) => {

    const account = new Accounts({
        number: body.number,
        createDate: body.createDate,
        balance: body.balance,
        client: body.client,
    })

    const acc = await account.save();
    const client = await Client.findById(acc.client);
    client.accounts.push(acc._id);
    await Client.updateOne({ _id: client._id }, client);

    return account;
}

module.exports = {
    getAll,
    getOne,
    count,
    save,
};