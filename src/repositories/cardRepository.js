const Card = require('../models/cards');
const Client = require('../models/clients');

const getAll = async () => await Card.find();
const getOne = async (id) => await Card.findById(id);
const count = async () => await Card.count();

const save = async (body) => {

    const card = new Card({
        cardHolder: body.cardHolder,
        type: body.type,
        color: body.color,
        number: body.number,
        cvv: body.cvv,
        thruDate: body.thruDate,
        fromDate: body.fromDate,
        client: body.client,
    })

    const tarjeta = await card.save();
    const client = await Client.findById(tarjeta.client);
    client.cards.push(tarjeta._id);
    await Client.updateOne({ _id: client._id }, client);
    return card;
}

module.exports = {
    getAll,
    getOne,
    count,
    save,
};