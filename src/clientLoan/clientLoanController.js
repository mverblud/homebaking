const getAll = require('../repositories/user/userCase/getClientLoan/getClientLoan');
const createClient = require('../repositories/user/userCase/createClientLoan/createClientLoan');

module.exports = {
    getAll,
    createClient,
}